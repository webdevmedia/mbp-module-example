<?php
namespace ModuleBasedPlugin\Module\ExampleModule;

use ModuleBasedPlugin\Factories,
	ModuleBasedPlugin\Handler;

class Config extends Factories\Abstracts\Entity  {

	/**
	 * @var Handler\Common
	 */
	private $common;
	
	/**
	 * @var Handler\pluginConfig
	 */
	private $pluginConfig;

	/**
	 * @var string
	 */
	private $moduleName = '';

	private $moduleBasePath = '';

	/**
	 * Config constructor.
	 *
	 * @param Handler\pluginConfig $pluginConfig
	 */
	public function __construct(Handler\pluginConfig $pluginConfig) {
		$this->common = new Handler\Common();

		$this->pluginConfig = $pluginConfig;

		$this->moduleName = $this->common->callee(1, 0, true)->getModuleName();
		$this->moduleBasePath = $this->common->callee(1, 0, true)->getModulePath();

		$this->setAssets();

		return $this;
	}

	public function setAssets(){
		$entitiesPath = dirname(__FILE__) . '/Configs';
		$this->loadEntities($entitiesPath, 'json', true);

		$moduleConfig = $this->get();

		$this->assets = $this->pluginConfig->enqueue($this->moduleName, $moduleConfig['enqueue'], $this->moduleBasePath );

		return $this;
	}


}
