<?php
namespace ModuleBasedPlugin\Module\ExampleModule;

use ModuleBasedPlugin\Factories;

class Tasks extends Factories\Abstracts\Tasks {

	public function __construct() {

		$this->add( 'exampleTask', ['doFoo', 'Bar'] );

	}

	public function load() {
        $this->initialize( 'tasks', dirname(__FILE__), __NAMESPACE__, false);
	}

}
