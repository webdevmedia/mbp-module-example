<?php
namespace ModuleBasedPlugin\Module\ExampleModule;

class Common {

	public static function exampleCommon(array $foo): bool{
		$bool = false;

		if(!empty($foo)){
			$bool = true;
		}

		return $bool;
	}

}
